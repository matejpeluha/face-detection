from tkinter import filedialog
import numpy as np
import cv2 as cv
import os
from mtcnn.mtcnn import MTCNN
import math


class VideoReader:
    def __init__(self):
        self.file = None
        self.npz = None
        self.video_writer = None
        self.imageType = ''
        self.face_detection = ''
        self.cascade = cv.CascadeClassifier('./data/haarcascade_frontalface_default.xml')
        self.detector = MTCNN()
        self.tp = 0
        self.fp = 0
        self.fn = 1
        self.left_center = [(0, 0), (0, 0)]
        self.right_center = [(0, 0), (0, 0)]

    def restart_results(self):
        self.tp = 0
        self.fp = 0
        self.fn = 1

    """RUN"""

    def run(self):
        self.load_video()
        if self.npz is None:
            return

        for face_detection in ['viola_jones', 'mtcnn']:
            self.face_detection = face_detection
            for imageType in ['original', 'medium', 'severe']:
                self.imageType = imageType
                self.create_video_writer()
                self.create_video()

    """LOAD/CREATE"""

    def load_video(self):
        self.file = filedialog.askopenfile(filetypes=[('NPZ', '*.npz')], initialdir='./sources')
        if self.file is None:
            return
        self.npz = np.load(self.file.name)

    def create_video_writer(self):
        directory = './' + self.file.name.split('/')[-1].replace('.npz', '/')
        if not os.path.isdir(directory):
            os.mkdir(directory)
        directory += self.face_detection + '/'
        if not os.path.isdir(directory):
            os.mkdir(directory)
        video_name = directory + self.file.name.split('/')[-1].replace('.npz', '_' + self.imageType + '.avi')
        fourcc = cv.VideoWriter_fourcc(*'DIVX')
        self.video_writer = cv.VideoWriter(
            video_name,
            fourcc,
            24,
            (self.npz['colorImages_' + self.imageType].shape[1],
             self.npz['colorImages_' + self.imageType].shape[0])
        )
        print('START ', video_name)

    def create_video(self):
        original_images = self.npz['colorImages_' + self.imageType]
        bounding_boxes = self.npz['boundingBox']
        all_landmarks = self.npz['landmarks2D']
        for i in range(original_images.shape[-1]):
            image = cv.cvtColor(np.ascontiguousarray(original_images[..., i], dtype=np.uint8), cv.COLOR_RGB2BGR)
            box = bounding_boxes[..., i]
            landmarks = all_landmarks[..., i]
            self.calculate_eyes_centers(landmarks)
            self.write_mask(image, box, landmarks)
            self.write_detected_faces(image, box)
            image = self.write_stats(image)
            self.video_writer.write(image)
        self.video_writer.release()

    """WRITE BOXES"""

    def write_mask(self, image, box, landmarks):
        cv.rectangle(
            image,
            (int(box[0][0]), int(box[0][1])),
            (int(box[3][0]), int(box[3][1])),
            color=(255, 0, 0),
            thickness=2
        )

        for i in range(landmarks.shape[0]):
            cv.circle(
                image,
                (int(landmarks[i][0]), int(landmarks[i][1])),
                radius=1,
                color=(255, 0, 0),
                thickness=1
            )

        cv.circle(image, self.left_center[0], radius=1, color=(255, 0, 0), thickness=2)
        cv.circle(image, self.right_center[0], radius=1, color=(255, 0, 0), thickness=2)

    def calculate_eyes_centers(self, landmarks):
        left_x = int((int(landmarks[36][0]) + int(landmarks[39][0])) / 2)
        left_y = int((int(landmarks[36][1]) + int(landmarks[39][1])) / 2)
        self.left_center[0] = (left_x, left_y)
        right_x = int((int(landmarks[42][0]) + int(landmarks[45][0])) / 2)
        right_y = int((int(landmarks[42][1]) + int(landmarks[45][1])) / 2)
        self.right_center[0] = (right_x, right_y)


    def write_detected_faces(self, image, box):
        self.restart_results()
        if self.face_detection == 'viola_jones':
            return self.use_viola_jones(image, box)
        elif self.face_detection == 'mtcnn':
            return self.use_mtcnn(image, box)

    """RECOGNITION"""

    def use_viola_jones(self, image, box):
        """
        Source: https://medium.com/0xcode/the-viola-jones-face-detection-algorithm-3eb09055cfc2
        """
        can_accept = True
        # faces = self.cascade.detectMultiScale(image, scaleFactor=1.3, minSize=(30, 30))
        faces = self.cascade.detectMultiScale(image)
        for face in faces:
            self.write_face_box(image, face, box, can_accept)

        return faces

    def use_mtcnn(self, image, box):
        """
        Multi-task Cascaded Convolutional Networks
        https://machinelearningmastery.com/how-to-perform-face-detection-with-classical-and-deep-learning-methods-in-python-with-keras/
        """
        can_accept = True
        faces = self.detector.detect_faces(image)
        for face in faces:
            self.write_face_box(image, face['box'], box, can_accept)
            self.write_detected_points(image, face['keypoints'])

        return faces

    def write_face_box(self, image, box, true_box, can_accept):
        column, row, width, height = box
        if can_accept and self.calculate_iou(box, true_box) > 0.5:
            color = (0, 255, 0)
            self.tp += 1
            self.fn -= 1
            can_accept = False
        else:
            color = (0, 0, 255)
            self.fp += 1
        cv.rectangle(
            image,
            (column, row),
            (column + width, row + height),
            color=color,
            thickness=2
        )

    def write_detected_points(self, image, points):
        nose = points['nose']
        right_mouth = points['mouth_right']
        left_mouth = points['mouth_left']
        right_eye = points['right_eye']
        left_eye = points['left_eye']
        self.left_center[1] = left_eye
        self.right_center[1] = right_eye
        if nose:
            cv.circle(image, nose, radius=1, color=(0, 255, 0), thickness=1)
        if right_mouth:
            cv.circle(image, right_mouth, radius=1, color=(0, 255, 0), thickness=1)
        if left_mouth:
            cv.circle(image, left_mouth, radius=1, color=(0, 255, 0), thickness=1)
        if right_eye:
            cv.circle(image, right_eye, radius=1, color=(0, 255, 0), thickness=1)
        if left_eye:
            cv.circle(image, left_eye, radius=1, color=(0, 255, 0), thickness=1)

    def write_stats(self, image):
        text1 = 'TP: ' + str(self.tp) + \
                ' FP: ' + str(self.fp) + \
                ' FN: ' + str(self.fn)
        if self.tp + self.fp == 0:
            text2 = 'Precision: 0.0'
        else:
            text2 = 'Precision: ' + str(self.tp / (self.tp + self.fp))
        text3 = 'Recall: ' + str(self.tp / (self.tp + self.fn))


        image = cv.putText(image, text1, (5, 10), cv.FONT_HERSHEY_SIMPLEX, 0.4, (255, 255, 255))
        image = cv.putText(image, text2, (5, 25), cv.FONT_HERSHEY_SIMPLEX, 0.4, (255, 255, 255))
        image = cv.putText(image, text3, (5, 40), cv.FONT_HERSHEY_SIMPLEX, 0.4, (255, 255, 255))
        if self.face_detection == 'mtcnn':
            image = self.write_mse(image)
        return image

    def write_mse(self, image):
        left_dist = math.dist(self.left_center[0], self.left_center[1])
        right_dist = math.dist(self.right_center[0], self.right_center[1])
        mse = (left_dist + right_dist) / 2
        text = 'MSE: ' + str(mse)
        return cv.putText(image, text, (5, 55), cv.FONT_HERSHEY_SIMPLEX, 0.4, (255, 255, 255))

    """INTERSECTION OVER UNION"""

    def calculate_iou(self, box, answer_box):
        """
        Source: https://pyimagesearch.com/2016/11/07/intersection-over-union-iou-for-object-detection/
        """
        intersection = self.get_intersection(box, answer_box)
        union = self.get_union(box, answer_box, intersection)

        return intersection / union

    def get_intersection(self, box, answer_box):
        xA = max(box[0], answer_box[0][0])
        yA = max(box[1], answer_box[0][1])
        xB = min(box[0] + box[2], answer_box[3][0])
        yB = min(box[1] + box[3], answer_box[3][1])

        return max(0, xB - xA + 1) * max(0, yB - yA + 1)

    def get_union(self, box, answer_box, intersection):
        boxAArea = (box[2] + 1) * (box[3] + 1)
        boxBArea = (answer_box[3][0] - answer_box[0][0] + 1) * (answer_box[3][1] - answer_box[0][1] + 1)

        return float(boxAArea + boxBArea - intersection)
